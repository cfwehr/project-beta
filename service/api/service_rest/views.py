from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import (
    TechnicianListEncoder, TechnicianDetailEncoder,
    AppointmentListEncoder, AppointmentDetailEncoder
)
from .models import Technician, Appointment
import json


@require_http_methods(['GET', 'POST'])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {'technicians': technician},
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            {'technicians': technician},
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'DELETE'])
def api_detail_technicians(request, id):
    if request.method == "GET":
        try:
            tech = Technician.objects.get(id=id)
        except Technician.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid Technician ID'},
                status=400,
            )
        return JsonResponse(
            {'technician': tech},
            encoder=TechnicianDetailEncoder,
            safe=False
        )
    else:
        count, _ = Technician.objects.filter(id=id).delete()
        if count == 0:
            return JsonResponse(
                {'message': f'Technician with ID \'{id}\' does not exist'},
                status=404,
            )
        return JsonResponse(
            {'message': f'Successfully deleted Technician with ID \'{id}\''},
            status=200,
        )


@require_http_methods(['GET', 'POST'])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            tech_id = content['technician']
            tech = Technician.objects.get(employee_id=tech_id)
            content['technician'] = tech
        except Technician.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid Technician ID'},
                status=400)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentListEncoder,
            safe=False
        )


@require_http_methods(['GET', 'DELETE'])
def api_detail_appointment(request, id):
    if request.method == "GET":
        try:
            appt = Appointment.objects.filter(id=id)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid Appointment ID'},
                status=400,
            )
        return JsonResponse(
            {'appointment': appt},
            encoder=AppointmentDetailEncoder,
            safe=False
        )
    else:
        count, _ = Appointment.objects.filter(id=id).delete()
        if count == 0:
            return JsonResponse(
                {'message': f'Appointment with ID \'{id}\' does not exist'},
                status=404,
            )
        return JsonResponse(
            {'message': f'Successfully deleted Appointment with ID \'{id}\''},
            status=200,
        )


@require_http_methods(['PUT'])
def api_cancel_appointment(request, id):
    try:
        appt = Appointment.objects.get(id=id)
        appt.cancel()
    except Appointment.DoesNotExist:
        return JsonResponse(
            {'message': 'Invalid Appointment ID'},
            status=400,
        )
    return JsonResponse(
        {'canceled': appt},
        encoder=AppointmentDetailEncoder,
        safe=False,
        status=200
    )


@require_http_methods(['PUT'])
def api_complete_appointment(request, id):
    try:
        appt = Appointment.objects.get(id=id)
        appt.finish()
    except Appointment.DoesNotExist:
        return JsonResponse(
            {'message': 'Invalid Appointment ID'},
            status=400,
        )
    return JsonResponse(
        {'finished': appt},
        encoder=AppointmentDetailEncoder,
        safe=False,
        status=200
    )
