from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Salesperson(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    employee_id = models.IntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"pk": self.id})


class Customer(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO, related_name="sales", on_delete=models.CASCADE
    )
    salesperson = models.ForeignKey(
        Salesperson, related_name="sales", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer, related_name="sales", on_delete=models.CASCADE
    )
    price = models.IntegerField()

    def get_api_url(self):
        return reverse("api_sale", kwargs={"pk": self.id})
