# EVOLUTION

## Description

EVOLUTION is a tailored application for electric automobile dealership management. It simplifies inventory, service, and sales operations for dealerships venturing into the electric vehicle market, while providing customers with a user-friendly interface to browse inventory, meet the team, and schedule services.

## Key Features

### Consumer Perspective:

- **Browse Inventory**: View the latest electric vehicles available for purchase.

- **Meet the Team**: Get to know the dealership's staff and their expertise.

- **Service Scheduling**: Schedule maintenance or service appointments with ease.

### Dealership Perspective:

- **Inventory Management**: Effortlessly manage and update electric vehicle stock, monitor inventory levels, and track vehicle sales.

- **Service Scheduling**: Oversee and manage service appointments, allocate technicians, and manage service history.

- **Sales Operations**: A comprehensive system to handle sales, from customer inquiries and test drives to finalizing sales deals and after-sales support.

- **Customer Database**: Maintain a rich database of customers, their purchase history, service requests, and more, ensuring personalized and efficient customer interactions.

## Why EVOLUTION?

The electric vehicle market is expanding rapidly, and EVOLUTION ensures that automobile dealerships, whether exclusively electric or hybrid, have the right tools to navigate this new terrain. By offering features tailored for both consumers and dealership operations, EVOLUTION is a comprehensive solution for the modern automobile dealership.

## Intended Audience

- **Consumers**: Individuals exploring electric vehicle options, wanting to schedule services, or learn more about specific models.

- **Electric Vehicle Dealerships**: Dealerships dedicated to the sale of electric vehicles looking for a unified management solution.

- **Hybrid Dealerships**: Traditional dealers transitioning to or expanding within the electric vehicle domain, looking for tools to simplify operations.

- **Service Centers**: Service hubs wanting an efficient system for appointment scheduling and service management.

## Functionality

On accessing EVOLUTION, consumers are welcomed with an intuitive interface to explore vehicles, schedule services, or get in touch with the dealership. Dealers, after logging in, are presented with a comprehensive dashboard that provides an at-a-glance view of ongoing operations. Navigation is straightforward, ensuring easy transitions between inventory updates, sales tracking, and service management modules.

## Initialization

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run docker volume create beta-data
4. Run docker-compose build
5. Run docker-compose up
6. Experience EVOLUTION in all its capacity!

## Future Development

As EVOLUTION evolves, we're setting our sights on:

Customer Feedback System: An integrated platform for customers to provide reviews and ratings for vehicles and services.

Authentication and Role-Based Access: Secure, role-specific access for dealership employees, safeguarding sensitive information, while providing customers with a personalized, yet restricted view tailored to their needs.

## Contributors

**Corissa Wehr**
<br></br>
**MK Grissom**

## Design

![Alt text](design.png)
