import { Container, Row, Col } from "react-bootstrap";

export const Footer = () => {
  return (
    <Container>
      <Row>
        <Col>
          <div className='footer'>
            <p>EVOLUTION</p>
            <p>1234 Main Street</p>
            <p>Somewhere, USA</p>
            <p>123-456-789</p>
          </div>
        </Col>
      </Row>
    </Container>
  );
};
