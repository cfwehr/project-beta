import React, { useState, useEffect } from "react";

async function loadData() {
  const response = await fetch(
    "https://mar-4-et-inventory.mod3projects.com/api/automobiles/"
  );
  if (response.ok) {
    const modelData = await response.json();
    return modelData;
  } else {
    console.error("Failed to fetch automobile data");
    return null;
  }
}

function AutomobileList() {
  const [autoData, setAutoData] = useState([]);

  useEffect(() => {
    loadData().then((data) => {
      setAutoData(data.autos);
    });
  }, []);

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Automobiles</h3>
        <table className='table'>
          <thead>
            <tr>
              <th className='text-center'>VIN</th>
              <th className='text-center'>Color</th>
              <th className='text-center'>Year</th>
              <th className='text-center'>Model</th>
              <th className='text-center'>Manufacturer</th>
              <th className='text-center'>Sold</th>
            </tr>
          </thead>
          <tbody>
            {autoData.length === 0 ? (
              <tr>
                <td colSpan='6' className='text-center'>
                  No automobile data available
                </td>
              </tr>
            ) : (
              autoData.map((auto) => {
                return (
                  <tr key={auto.id}>
                    <td className='text-center'>{auto.vin}</td>
                    <td className='text-center'>{auto.color}</td>
                    <td className='text-center'>{auto.year}</td>
                    <td className='text-center'>{auto.model.name}</td>
                    <td className='text-center'>
                      {auto.model.manufacturer.name}
                    </td>
                    <td className='text-center'>{auto.sold ? "Yes" : "No"}</td>
                  </tr>
                );
              })
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default AutomobileList;
