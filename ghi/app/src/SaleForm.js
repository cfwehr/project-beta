import React, { useState, useEffect } from "react";

function SaleForm() {
  const [automobiles, setAutomobiles] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [selectedAutomobile, setSelectedAutomobile] = useState("");
  const [selectedSalesperson, setSelectedSalesperson] = useState("");
  const [selectedCustomer, setSelectedCustomer] = useState("");
  const [price, setPrice] = useState("");

  useEffect(() => {
    const fetchSalesAndAutomobiles = async () => {
      try {
        const salesResponse = await fetch(
          "https://mar-4-et-sales.mod3projects.com/api/sales/"
        );
        const salesData = await salesResponse.json();
        const soldAutomobiles = salesData.sales.map(
          (sale) => sale.automobile.vin
        );
        const inventoryResponse = await fetch(
          "https://mar-4-et-inventory.mod3projects.com/api/automobiles/"
        );
        const inventoryData = await inventoryResponse.json();
        const unsoldAutomobiles = inventoryData.autos.filter(
          (automobile) => !soldAutomobiles.includes(automobile.vin)
        );
        setAutomobiles(unsoldAutomobiles);
      } catch (error) {
        console.error("Error fetching sales and automobiles:", error);
      }
    };

    fetchSalesAndAutomobiles();

    fetch("https://mar-4-et-sales.mod3projects.com/api/salespeople/")
      .then((response) => response.json())
      .then((data) => {
        setSalespeople(data.salespeople);
      })
      .catch((error) => {
        console.error("Error fetching salespeople:", error);
      });

    fetch("https://mar-4-et-sales.mod3projects.com/api/customers/")
      .then((response) => response.json())
      .then((data) => {
        setCustomers(data.customers);
      })
      .catch((error) => {
        console.error("Error fetching customers:", error);
      });
  }, []);

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setSelectedAutomobile(value);
  };
  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSelectedSalesperson(value);
  };
  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setSelectedCustomer(value);
  };
  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const sale = {
      automobile: selectedAutomobile,
      salesperson: selectedSalesperson,
      customer: selectedCustomer,
      price: price,
    };

    const salesURL = "https://mar-4-et-sales.mod3projects.com/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(sale),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salesURL, fetchConfig);
    const autoURL = `https://mar-4-et-inventory.mod3projects.com/api/automobiles/${selectedAutomobile}/`;
    const autoSale = {
      sold: true,
    };
    const fConfig = {
      method: "PUT",
      body: JSON.stringify(autoSale),
      headers: {
        "Content-Type": "application/json",
      },
    };
    await fetch(autoURL, fConfig);

    if (response.ok) {
      await response.json();
      setSelectedAutomobile("");
      setSelectedSalesperson("");
      setSelectedCustomer("");
      setPrice("");

      setAutomobiles((prevAutomobiles) => {
        return prevAutomobiles.filter(
          (automobile) => automobile.vin !== selectedAutomobile
        );
      });
    }
  };

  return (
    <div className='form-container'>
      <div className='form-card'>
        <form onSubmit={handleSubmit} id='create-sale-form'>
          <h3 className='form-title'>New Sale</h3>

          <div className='input-container'>
            <div className='input-floating'>
              <select
                onChange={handleAutomobileChange}
                required
                id='automobile'
                name='automobile'
                className='form-input'
                value={selectedAutomobile}
              >
                <option value=''>Select an automobile</option>
                {automobiles.map((automobile) => (
                  <option key={automobile.id} value={automobile.vin}>
                    {automobile.vin}
                  </option>
                ))}
              </select>
              <label htmlFor='automobile'>Automobile VIN</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <select
                onChange={handleSalespersonChange}
                required
                id='salesperson'
                name='salesperson'
                className='form-input'
                value={selectedSalesperson}
              >
                <option value=''>Select a salesperson</option>
                {salespeople.map((salesperson) => (
                  <option key={salesperson.id} value={salesperson.employee_id}>
                    {salesperson.first_name} {salesperson.last_name}
                  </option>
                ))}
              </select>
              <label htmlFor='salesperson'>Salesperson</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <select
                onChange={handleCustomerChange}
                required
                id='customer'
                name='customer'
                className='form-input'
                value={selectedCustomer}
              >
                <option value=''>Select a customer</option>
                {customers.map((customer) => (
                  <option key={customer.id} value={customer.id}>
                    {customer.first_name} {customer.last_name}
                  </option>
                ))}
              </select>
              <label htmlFor='customer'>Customer</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handlePriceChange}
                required
                placeholder='$0.00'
                type='text'
                id='price'
                name='price'
                className='form-input'
                value={price}
              />
              <label htmlFor='price'>Price</label>
            </div>
          </div>

          <div className='submit-container'>
            <button className='submit-button'>Create</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default SaleForm;
