import React, { useState, useEffect } from "react";

function AutomobileForm() {
  const [color, setColor] = useState("");
  const [year, setYear] = useState("");
  const [vin, setVin] = useState("");
  const [model, setModel] = useState("");
  const [models, setModels] = useState([]);

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handleYearChange = (event) => {
    const value = event.target.value;
    setYear(value);
  };

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleModelChange = (event) => {
    const value = event.target.value;
    setModel(value);
  };

  const fetchData = async () => {
    const url = "https://mar-4-et-inventory.mod3projects.com/api/models/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.color = color;
    data.year = year;
    data.vin = vin;
    data.model_id = model;
    const autoUrl =
      "https://mar-4-et-inventory.mod3projects.com/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(autoUrl, fetchConfig);
    if (response.ok) {
      setColor("");
      setYear("");
      setVin("");
      setModel("");
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className='form-container'>
      <div className='form-card'>
        <form onSubmit={handleSubmit} id='create-automobile-form'>
          <h3 className='form-title'>New Automobile</h3>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleColorChange}
                required
                placeholder='Color'
                type='text'
                id='color'
                name='color'
                className='form-input'
                value={color}
              />
              <label htmlFor='color'>Color</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleYearChange}
                required
                placeholder='Year'
                type='text'
                id='year'
                name='year'
                minLength='4'
                maxLength='4'
                className='form-input'
                value={year}
                onKeyDown={(event) => {
                  if (
                    !/[0-9]/.test(event.key) &&
                    event.key !== "Backspace" &&
                    event.key !== "Delete"
                  ) {
                    event.preventDefault();
                  }
                }}
              />
              <label htmlFor='year'>Year</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleVinChange}
                required
                placeholder='VIN'
                type='text'
                id='vin'
                name='vin'
                minLength='17'
                maxLength='17'
                className='form-input'
                value={vin}
              />
              <label htmlFor='vin'>VIN</label>
            </div>
          </div>

          <div className='input-container'>
            <select
              onChange={handleModelChange}
              name='model'
              id='model'
              required
              value={model}
              className='form-select mb-3'
            >
              <option value=''>Choose a Model</option>
              {models.map((model) => (
                <option key={model.id} value={model.id}>
                  {" "}
                  {model.name}{" "}
                </option>
              ))}
            </select>
          </div>

          <div className='submit-container'>
            <button className='submit-button'>Create</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AutomobileForm;
