import React, { useState, useEffect } from "react";

async function loadData() {
  const response = await fetch(
    "https://mar-4-et-service.mod3projects.com/api/appointments/"
  );
  if (response.ok) {
    const data = await response.json();
    return data.appointments || [];
  } else {
    console.error("Failed to fetch appointment data");
    return [];
  }
}

function AppointmentHistory() {
  const [apptData, setApptData] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    loadData().then((appointments) => {
      setApptData(appointments);
    });
  }, []);

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
  };

  const filteredAppointments = apptData.filter((appt) =>
    appt.vin.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Service History</h3>
        <input
          type='text'
          placeholder='Search by VIN...'
          value={searchTerm}
          onChange={handleSearch}
          className='table-input mb-3'
        />
        <table className='table'>
          <thead>
            <tr>
              <th className='text-center'>VIN</th>
              <th className='text-center'>VIP</th>
              <th className='text-center'>Customer</th>
              <th className='text-center'>Date</th>
              <th className='text-center'>Time</th>
              <th className='text-center'>Technician</th>
              <th className='text-center'>Reason</th>
              <th className='text-center'>Status</th>
            </tr>
          </thead>
          <tbody>
            {filteredAppointments.length === 0 ? (
              <tr>
                <td colSpan='8' className='text-center'>
                  No service history data available
                </td>
              </tr>
            ) : (
              filteredAppointments.map((appt) => {
                const dateTime = new Date(appt.date_time);
                const date = dateTime.toLocaleDateString("en-US", {
                  month: "2-digit",
                  day: "2-digit",
                  year: "numeric",
                });
                const time = dateTime.toLocaleTimeString("en-US", {
                  hour: "2-digit",
                  minute: "2-digit",
                  hour12: true,
                });
                return (
                  <tr key={appt.id}>
                    <td className='text-center'>{appt.vin}</td>
                    <td className='text-center'>
                      {String(appt.is_vip).charAt(0).toUpperCase() +
                        String(appt.is_vip).slice(1)}
                    </td>
                    <td className='text-center'>{appt.customer}</td>
                    <td className='text-center'>{date}</td>
                    <td className='text-center'>{time}</td>
                    <td className='text-center'>
                      {appt.technician.first_name} {appt.technician.last_name}
                    </td>
                    <td className='text-center'>{appt.reason}</td>
                    <td className='text-center'>{appt.status}</td>
                  </tr>
                );
              })
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default AppointmentHistory;
