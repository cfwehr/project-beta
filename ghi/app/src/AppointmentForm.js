import React, { useState, useEffect } from "react";

function AppointmentForm() {
  const [dateTime, setDateTime] = useState("");
  const [reason, setReason] = useState("");
  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [technician, setTechnician] = useState("");
  const [technicians, setTechnicians] = useState([]);

  const handleDateTimeChange = (event) => {
    const value = event.target.value;
    setDateTime(value);
  };

  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  const fetchData = async () => {
    const url = "https://mar-4-et-service.mod3projects.com/api/technicians/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.date_time = dateTime;
    data.reason = reason;
    data.vin = vin;
    data.customer = customer;
    data.technician = technician;
    const apptUrl =
      "https://mar-4-et-service.mod3projects.com/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(apptUrl, fetchConfig);
    if (response.ok) {
      setDateTime("");
      setReason("");
      setVin("");
      setCustomer("");
      setTechnician("");
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className='form-container'>
      <div className='form-card'>
        <form onSubmit={handleSubmit} id='create-appt-form'>
          <h3 className='form-title'>New Appointment</h3>

          <select
            onChange={handleTechnicianChange}
            name='technician'
            id='technician'
            required
            value={technician}
            className='form-select mb-3'
          >
            <option value=''>Choose a Technician</option>
            {technicians.map((technician) => (
              <option
                key={technician.employee_id}
                value={technician.employee_id}
              >
                {" "}
                {technician.first_name} {technician.last_name}{" "}
              </option>
            ))}
          </select>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleVinChange}
                required
                placeholder='VIN'
                type='text'
                id='vin'
                name='vin'
                className='form-input'
                minLength='17'
                maxLength='17'
                value={vin}
              />
              <label htmlFor='vin'>Automobile VIN</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleCustomerChange}
                required
                placeholder='Customer'
                type='text'
                id='customer'
                name='customer'
                className='form-input'
                value={customer}
              />
              <label htmlFor='customer'>Customer</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleDateTimeChange}
                required
                type='datetime-local'
                id='dateTime'
                name='dateTime'
                className='form-input'
                value={dateTime}
              />
              <label htmlFor='dateTime'>Date Time</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleReasonChange}
                required
                placeholder='Reason'
                type='text'
                id='reason'
                name='reason'
                className='form-input'
                value={reason}
              />
              <label htmlFor='reason'>Reason</label>
            </div>
          </div>

          <div className='submit-container'>
            <button className='submit-button'>Create</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AppointmentForm;
