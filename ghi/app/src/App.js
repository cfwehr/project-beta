import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SalespersonForm from "./SalespersonForm";
import SalespersonList from "./SalespersonList";
import CustomerList from "./CustomerList";
import CustomerForm from "./CustomerForm";
import SaleList from "./SaleList";
import SaleForm from "./SaleForm";
import SaleHistory from "./SaleHistory";
import TechnicianForm from "./TechnicianForm";
import AppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentList";
import AppointmentHistory from "./AppointmentHistory";
import TechnicianList from "./TechnicianList";
import ModelForm from "./ModelForms";
import ModelList from "./ModelList";
import AutomobileList from "./AutomobileList";
import AutomobileForm from "./AutomobileForm";
import ManufacturerList from "./ManufacturerList";
import ManufacturerForm from "./ManufacturerForm";
import "./App.css";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <div className='container'>
        <Routes>
          <Route path='/' element={<MainPage />} />
          <Route path='salespeople'>
            <Route path='' element={<SalespersonList />} />
            <Route path='new' element={<SalespersonForm />} />
          </Route>
          <Route path='customers'>
            <Route path='' element={<CustomerList />} />
            <Route path='new' element={<CustomerForm />} />
          </Route>
          <Route path='sales'>
            <Route path='' element={<SaleList />} />
            <Route path='new' element={<SaleForm />} />
            <Route path='history' element={<SaleHistory />} />
          </Route>
          <Route path='technicians'>
            <Route path='' element={<TechnicianList />} />
            <Route path='new' element={<TechnicianForm />} />
          </Route>
          <Route path='appointments'>
            <Route path='' element={<AppointmentList />} />
            <Route path='history' element={<AppointmentHistory />} />
            <Route path='new' element={<AppointmentForm />} />
          </Route>
          <Route path='models'>
            <Route path='' element={<ModelList />} />
            <Route path='new' element={<ModelForm />} />
          </Route>
          <Route path='automobiles'>
            <Route path='' element={<AutomobileList />} />
            <Route path='new' element={<AutomobileForm />} />
          </Route>
          <Route path='manufacturers'>
            <Route path='' element={<ManufacturerList />} />
            <Route path='new' element={<ManufacturerForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
