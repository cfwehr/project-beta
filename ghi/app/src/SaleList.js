import React, { useState, useEffect } from "react";

export default function SaleList() {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    fetchSales();
  }, []);

  const fetchSales = async () => {
    const response = await fetch(
      "https://mar-4-et-sales.mod3projects.com/api/sales/"
    );
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      console.error("Error fetching sales:", response.status);
    }
  };

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Sales</h3>
        <table className='table'>
          <thead>
            <tr>
              <th className='text-center'></th>
              <th className='text-center'>VIN</th>
              <th className='text-center'>Salesperson</th>
              <th className='text-center'>Customer</th>
              <th className='text-center'>Price</th>
            </tr>
          </thead>
          <tbody>
            {sales.length === 0 ? (
              <tr>
                <td colSpan='5' className='text-center'>
                  No sale data available
                </td>
              </tr>
            ) : (
              sales.map((sale, index) => (
                <tr key={sale.id}>
                  <th className='text-center' scope='row'>
                    {index + 1}
                  </th>
                  <td className='text-center'>{sale.automobile.vin}</td>
                  <td className='text-center'>{`${sale.salesperson.first_name} ${sale.salesperson.last_name} (${sale.salesperson.employee_id})`}</td>
                  <td className='text-center'>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                  <td className='text-center'>{"$" + sale.price}</td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}
