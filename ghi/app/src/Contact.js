import React, { useState } from "react";

function ContactForm() {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    message: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData);
    setFormData({
      name: "",
      email: "",
      message: "",
    });
  };

  return (
    <div className='contact' id='contact'>
      <h1>Contact Us</h1>
      <form onSubmit={handleSubmit}>
        <div className='field'>
          <label htmlFor='name'>Name</label>
          <input
            type='text'
            id='name'
            name='name'
            placeholder='Enter your first & last name...'
            value={formData.name}
            onChange={handleChange}
            required
          />
        </div>
        <div className='field'>
          <label htmlFor='email'>Email</label>
          <input
            type='email'
            id='email'
            name='email'
            placeholder='Enter your email address...'
            value={formData.email}
            onChange={handleChange}
            required
          />
        </div>
        <div className='field'>
          <label htmlFor='message'>Message</label>
          <textarea
            id='message'
            name='message'
            placeholder='E.g., I would like to book a test drive for the Polestar 2.'
            value={formData.message}
            onChange={handleChange}
            required
          />
        </div>
        <button type='submit'>Submit</button>
      </form>
    </div>
  );
}

export default ContactForm;
