import React, { useState } from "react";

function ManufacturerForm() {
  const [manufacturer, setManufacturer] = useState("");

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name: manufacturer,
    };
    const manufacturerUrl =
      "https://mar-4-et-inventory.mod3projects.com/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      setManufacturer("");
    }
  };

  return (
    <div className='form-container'>
      <div className='form-card'>
        <form onSubmit={handleSubmit} id='create-manufacturer-form'>
          <h3 className='form-title'>New Manufacturer</h3>
          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleManufacturerChange}
                required
                placeholder='manufacturer'
                type='text'
                id='manufacturer'
                name='manufacturer'
                className='form-input'
                value={manufacturer}
              />
              <label htmlFor='manufacturer'>Manufacturer</label>
            </div>
          </div>
          <div className='submit-container'>
            <button className='submit-button'>Create</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default ManufacturerForm;
