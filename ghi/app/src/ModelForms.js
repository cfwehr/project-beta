import React, { useState, useEffect } from "react";

function ModelForm() {
  const [modelName, setModelName] = useState("");
  const [picture, setPicture] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [manufacturers, setManufacturers] = useState([]);

  const handleModelNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
  };

  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
  };

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  const fetchData = async () => {
    const url =
      "https://mar-4-et-inventory.mod3projects.com/api/manufacturers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = modelName;
    data.picture_url = picture;
    data.manufacturer_id = manufacturer;
    const modelUrl = "https://mar-4-et-inventory.mod3projects.com/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      setModelName("");
      setPicture("");
      setManufacturer("");
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className='form-container'>
      <div className='form-card'>
        <form onSubmit={handleSubmit} id='create-model-form'>
          <h3 className='form-title'>New Model</h3>

          <select
            onChange={handleManufacturerChange}
            name='manufacturer'
            id='manufacturer'
            required
            value={manufacturer}
            className='form-select mb-3'
          >
            <option value=''>Choose a Manufacturer</option>
            {manufacturers.map((manufacturer) => (
              <option key={manufacturer.id} value={manufacturer.id}>
                {" "}
                {manufacturer.name}{" "}
              </option>
            ))}
          </select>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleModelNameChange}
                required
                placeholder='Model'
                type='text'
                id='model'
                name='model'
                className='form-input'
                value={modelName}
              />
              <label htmlFor='model'>Model</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handlePictureChange}
                required
                placeholder='Image URL'
                type='url'
                id='picture'
                name='picture'
                className='form-input'
                value={picture}
              />
              <label htmlFor='picture'>Image</label>
            </div>
          </div>

          <div className='submit-container'>
            <button className='submit-button'>Create</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default ModelForm;
