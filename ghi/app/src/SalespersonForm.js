import React, { useState } from "react";

function SalespersonForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeId, setEmployeeId] = useState("");

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };
  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };
  const handleEmployeeIdChange = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const salesperson = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId,
    };

    const salespeopleURL =
      "https://mar-4-et-sales.mod3projects.com/api/salespeople/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(salesperson),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salespeopleURL, fetchConfig);
    if (response.ok) {
      setFirstName("");
      setLastName("");
      setEmployeeId("");
    }
  };

  return (
    <div className='form-container'>
      <div className='form-card'>
        <form onSubmit={handleSubmit} id='create-salesperson-form'>
          <h3 className='form-title'>New Salesperson</h3>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleFirstNameChange}
                required
                placeholder='First Name'
                type='text'
                id='first_name'
                name='first_name'
                className='form-input'
                value={firstName}
              />
              <label htmlFor='first_name'>First Name</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleLastNameChange}
                required
                placeholder='Last Name'
                type='text'
                id='last_name'
                name='last_name'
                className='form-input'
                value={lastName}
              />
              <label htmlFor='last_name'>Last Name</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleEmployeeIdChange}
                required
                placeholder='Employee ID'
                type='text'
                id='employee_id'
                name='employee_id'
                className='form-input'
                value={employeeId}
              />
              <label htmlFor='employee_id'>Employee ID</label>
            </div>
          </div>

          <div className='submit-container'>
            <button className='submit-button'>Create</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default SalespersonForm;
