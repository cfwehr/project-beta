import React, { useState, useEffect } from "react";

async function loadData() {
  const response = await fetch(
    "https://mar-4-et-service.mod3projects.com/api/appointments/"
  );
  if (response.ok) {
    const data = await response.json();
    return data.appointments || [];
  } else {
    console.error("Failed to fetch appointment data");
    return [];
  }
}

function AppointmentList() {
  const [apptData, setApptData] = useState([]);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    loadData().then((appointments) => {
      setApptData(appointments);
    });
  }, [refresh]);

  async function cancel(id) {
    try {
      const url = `https://mar-4-et-service.mod3projects.com/api/appointments/${id}/cancel/`;
      const fetchConfig = {
        method: "put",
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setRefresh(!refresh);
      }
    } catch (e) {
      console.error("Error cancelling appointment", e);
    }
  }

  async function complete(id) {
    try {
      const url = `https://mar-4-et-service.mod3projects.com/api/appointments/${id}/finish/`;
      const fetchConfig = {
        method: "put",
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setRefresh(!refresh);
      }
    } catch (e) {
      console.error("Error completing appointment", e);
    }
  }

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Appointments</h3>
        <table className='table'>
          <thead>
            <tr>
              <th className='text-center'>VIN</th>
              <th className='text-center'>VIP</th>
              <th className='text-center'>Customer</th>
              <th className='text-center'>Date</th>
              <th className='text-center'>Time</th>
              <th className='text-center'>Technician</th>
              <th className='text-center'>Reason</th>
              <th className='text-center'>Status</th>
              <th className='text-center'></th>
              <th className='text-center'></th>
            </tr>
          </thead>
          <tbody>
            {apptData.length === 0 ? (
              <tr>
                <td colSpan='10' className='text-center'>
                  No appointment data available
                </td>
              </tr>
            ) : (
              apptData.map((appt) => {
                const dateTime = new Date(appt.date_time);
                const date = dateTime.toLocaleDateString("en-US", {
                  month: "2-digit",
                  day: "2-digit",
                  year: "numeric",
                });
                const time = dateTime.toLocaleTimeString("en-US", {
                  hour: "2-digit",
                  minute: "2-digit",
                  hour12: true,
                });
                return (
                  <tr key={appt.id}>
                    <td className='text-center'>{appt.vin}</td>
                    <td className='text-center'>
                      {String(appt.is_vip).charAt(0).toUpperCase() +
                        String(appt.is_vip).slice(1)}
                    </td>
                    <td className='text-center'>{appt.customer}</td>
                    <td className='text-center'>{date}</td>
                    <td className='text-center'>{time}</td>
                    <td className='text-center'>
                      {appt.technician.first_name} {appt.technician.last_name}
                    </td>
                    <td className='text-center'>{appt.reason}</td>
                    <td className='text-center'>{appt.status}</td>
                    <td className='text-center'>
                      <button
                        className={`btn btn-danger ${
                          ["CANCELED", "FINISHED"].includes(appt.status)
                            ? "disabled"
                            : ""
                        }`}
                        onClick={() => {
                          if (!["CANCELED", "FINISHED"].includes(appt.status)) {
                            cancel(appt.id);
                          }
                        }}
                        disabled={["CANCELED", "FINISHED"].includes(
                          appt.status
                        )}
                      >
                        Cancel
                      </button>
                    </td>
                    <td className='text-center'>
                      <button
                        className={`btn btn-success ${
                          ["CANCELED", "FINISHED"].includes(appt.status)
                            ? "disabled"
                            : ""
                        }`}
                        onClick={() => {
                          if (!["CANCELED", "FINISHED"].includes(appt.status)) {
                            complete(appt.id);
                          }
                        }}
                        disabled={["CANCELED", "FINISHED"].includes(
                          appt.status
                        )}
                      >
                        Complete
                      </button>
                    </td>
                  </tr>
                );
              })
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default AppointmentList;
