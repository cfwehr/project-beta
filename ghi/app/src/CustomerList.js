import React, { useState, useEffect } from "react";

export default function CustomerList() {
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    fetchCustomers();
  }, []);

  const fetchCustomers = async () => {
    const response = await fetch(
      "https://mar-4-et-sales.mod3projects.com/api/customers/"
    );
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    } else {
      console.error("Error fetching customers:", response.status);
    }
  };

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Customers</h3>
        <table className='table'>
          <thead>
            <tr>
              <th className='text-center'></th>
              <th className='text-center'>First Name</th>
              <th className='text-center'>Last Name</th>
              <th className='text-center'>Address</th>
              <th className='text-center'>Phone Number</th>
            </tr>
          </thead>
          <tbody>
            {customers.length === 0 ? (
              <tr>
                <td colSpan='5' className='text-center'>
                  No customer data available
                </td>
              </tr>
            ) : (
              customers.map((customer, index) => (
                <tr key={customer.id}>
                  <th className='text-center' scope='row'>
                    {index + 1}
                  </th>
                  <td className='text-center'>{customer.first_name}</td>
                  <td className='text-center'>{customer.last_name}</td>
                  <td className='text-center'>{customer.address}</td>
                  <td className='text-center'>{customer.phone_number}</td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}
