import { Container, NavLink } from "react-bootstrap";
import Carousel from "react-bootstrap/Carousel";
import main1 from "./assets/images/main1.jpg";
import main2 from "./assets/images/main2.jpg";
import main3 from "./assets/images/main3.jpg";
import bmwLogo from "./assets/images/bmw-logo.png";
import teslaLogo from "./assets/images/tesla-logo.png";
import polestarLogo from "./assets/images/polestar-logo.png";
import rivianLogo from "./assets/images/rivian-logo.png";
import { Footer } from "./Footer";
import Contact from "./Contact";
import ScrollToTopButton from "./ScrollArrow";

function MainPage() {
  return (
    <Container>
      <div className='px-4 py-3 my-5 text-center'>
        <h1 className='custom-heading text-center'>
          <span className='first-letters'>EV</span>
          <span className='second-letters'>OLUTION</span>
        </h1>
        <div className='col-lg-6 mx-auto'>
          <p className='main-slogan'>
            Elevating Your Drive, from Purchase to Perfection
          </p>
        </div>
      </div>
      <div className='carousel-container'>
        <Carousel>
          <Carousel.Item interval={7000}>
            <img src={main1} text='First slide' alt='BMW i8' />
            <Carousel.Caption>
              <div className='carousel-caption-box'>
                <h3>The EVOLUTION Experience</h3>
                <p>
                  Embark on a sustainable journey with EVOLUTION, where each
                  drive echoes innovation.
                  <br></br> Explore our range of electric vehicles that redefine
                  your automotive experience.
                </p>
                <NavLink a href='/automobiles'>
                  View Inventory
                </NavLink>
              </div>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item interval={7000}>
            <img src={main2} text='Second slide' alt='Hyundai IONIQ 6' />
            <Carousel.Caption>
              <div className='carousel-caption-box'>
                <h3>Introducing Our Experts</h3>
                <p>
                  At EVOLUTION, we take pride in our team of experts who are
                  passionate about electric vehicles.
                  <br></br>Our team is ready to assist you on your journey
                  towards sustainable and innovative driving.
                </p>
                <NavLink a href='/salespeople'>
                  Meet the Team
                </NavLink>
              </div>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item interval={7000}>
            <img src={main3} text='Third slide' alt='Tesla interior' />
            <Carousel.Caption>
              <div className='carousel-caption-box'>
                <h3>Unmatched Service</h3>
                <p>
                  Beyond selling cars, EVOLUTION is your partner in maintaining
                  and enhancing your electric vehicle.<br></br>
                  Our expert service ensures your journey is always smooth,
                  efficient, and worry-free.
                </p>
                <NavLink a href='/appointments/new'>
                  Schedule a Service
                </NavLink>
              </div>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>
      <div className='px-4 py-5 my-5 text-center'>
        <div className='mission-box'>
          <h1>Vehicles We Service</h1>
          <Carousel
            className='owl-carousel owl-theme skill-slider'
            interval={2000}
          >
            <Carousel.Item>
              <div className='item'>
                <img src={teslaLogo} alt='Tesla' />
                <div className='text-container'>
                  <h5>Tesla</h5>
                </div>
              </div>
            </Carousel.Item>
            <Carousel.Item>
              <div className='item'>
                <img src={polestarLogo} alt='Polestar' />
                <div className='text-container'>
                  <h5>Polestar</h5>
                </div>
              </div>
            </Carousel.Item>
            <Carousel.Item>
              <div className='item'>
                <img src={rivianLogo} alt='Rivian' />
                <div className='text-container'>
                  <h5>Rivian</h5>
                </div>
              </div>
            </Carousel.Item>
            <Carousel.Item>
              <div className='item'>
                <img src={bmwLogo} alt='BMW' />
                <div className='text-container'>
                  <h5>BMW</h5>
                </div>
              </div>
            </Carousel.Item>
          </Carousel>
        </div>
      </div>
      <div className='px-4 py-5 my-5 text-center'>
        <div className='mission-box'>
          <h1>Our Mission</h1>
          <div className='col-lg-6 mx-auto'>
            <p className='mission-text'>
              At EVOLUTION, we are committed to providing you with the best
              electric vehicle experience. We are passionate about electric
              vehicles and are dedicated to helping you find the perfect vehicle
              for your lifestyle. Our team of experts are here to assist you on
              your journey towards sustainable and innovative driving.
            </p>
          </div>
        </div>
      </div>
      <Contact />
      <ScrollToTopButton />
      <Footer />
    </Container>
  );
}

export default MainPage;
