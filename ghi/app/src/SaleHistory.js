import React, { useState, useEffect } from "react";

export default function SaleList() {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [alertVisible, setAlertVisible] = useState(
    localStorage.getItem("alertVisible") === "true"
  );
  const [selectedSalesperson, setSelectedSalesperson] = useState("");

  useEffect(() => {
    fetchSales();
    fetchSalespeople();
  }, []);

  useEffect(() => {
    if (alertVisible) {
      setTimeout(() => {
        setAlertVisible(false);
        localStorage.setItem("alertVisible", "false");
      }, 2000);
    }
  }, [alertVisible]);

  const fetchSales = async () => {
    const response = await fetch(
      "https://mar-4-et-sales.mod3projects.com/api/sales/"
    );
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      console.error("Error fetching sales:", response.status);
    }
  };

  const fetchSalespeople = async () => {
    const response = await fetch(
      "https://mar-4-et-sales.mod3projects.com/api/salespeople/"
    );
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error("Error fetching salespeople:", response.status);
    }
  };

  const handleChangeSalesperson = (event) => {
    const value = event.target.value;
    setSelectedSalesperson(value);
  };

  const filteredSales = selectedSalesperson
    ? sales.filter(
        (sale) => sale.salesperson.employee_id === selectedSalesperson
      )
    : sales;

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Sales History</h3>
        <select
          value={selectedSalesperson}
          onChange={handleChangeSalesperson}
          className='table-select'
        >
          <option value=''>All salespeople</option>
          {salespeople.map((salesperson) => (
            <option key={salesperson.id} value={salesperson.employee_id}>
              {`${salesperson.first_name} ${salesperson.last_name}`}
            </option>
          ))}
        </select>
        <table className='table'>
          <thead>
            <tr>
              <th className='text-center'></th>
              <th className='text-center'>Salesperson</th>
              <th className='text-center'>Customer</th>
              <th className='text-center'>VIN</th>
              <th className='text-center'>Price</th>
            </tr>
          </thead>
          <tbody>
            {filteredSales.length === 0 ? (
              <tr>
                <td colSpan='4' className='text-center'>
                  No sale data available
                </td>
              </tr>
            ) : (
              filteredSales.map((sale, index) => (
                <tr key={sale.id}>
                  <th className='text-center' scope='row'>
                    {index + 1}
                  </th>
                  <td className='text-center'>{`${sale.salesperson.first_name} ${sale.salesperson.last_name} (${sale.salesperson.employee_id})`}</td>
                  <td className='text-center'>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                  <td className='text-center'>{sale.automobile.vin}</td>
                  <td className='text-center'>{"$" + sale.price}</td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}
