import React, { useState, useEffect } from "react";

async function loadData() {
  const response = await fetch(
    "https://mar-4-et-inventory.mod3projects.com/api/models/"
  );
  if (response.ok) {
    const modelData = await response.json();
    return modelData;
  } else {
    console.error("Failed to fetch model data");
    return null;
  }
}

function ModelList() {
  const [modelData, setModelData] = useState([]);

  useEffect(() => {
    loadData().then((data) => {
      setModelData(data.models);
    });
  }, []);

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Models</h3>
        <table className='table'>
          <thead>
            <tr>
              <th className='text-center'>Name</th>
              <th className='text-center'>Manufacturer</th>
              <th className='text-center'>Picture</th>
            </tr>
          </thead>
          <tbody>
            {modelData.length === 0 ? (
              <tr>
                <td colSpan='4' className='text-center'>
                  No model data available
                </td>
              </tr>
            ) : (
              modelData.map((model) => (
                <tr key={model.id}>
                  <td className='text-center'>{model.name}</td>
                  <td className='text-center'>{model.manufacturer.name}</td>
                  <td className='text-center'>
                    <img
                      src={model.picture_url}
                      height='100'
                      width='100'
                      className='img-thumbnail'
                      alt={`Model ${model.name}`}
                    />
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default ModelList;
