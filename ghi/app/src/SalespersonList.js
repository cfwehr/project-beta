import React, { useState, useEffect } from "react";

export default function SalespersonList() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    fetchSalespeople();
  }, []);

  const fetchSalespeople = async () => {
    const response = await fetch(
      "https://mar-4-et-sales.mod3projects.com/api/salespeople/"
    );
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error("Error fetching salespeople:", response.status);
    }
  };

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Salespeople</h3>
        <table className='table'>
          <thead>
            <tr>
              <th className='text-center'></th>
              <th className='text-center'>First Name</th>
              <th className='text-center'>Last Name</th>
              <th className='text-center'>Employee ID</th>
            </tr>
          </thead>
          <tbody>
            {salespeople.length === 0 ? (
              <tr>
                <td colSpan='4' className='text-center'>
                  No salesperson data available
                </td>
              </tr>
            ) : (
              salespeople.map((salesperson, index) => (
                <tr key={salesperson.employee_id}>
                  <th className='text-center' scope='row'>
                    {index + 1}
                  </th>
                  <td className='text-center'>{salesperson.first_name}</td>
                  <td className='text-center'>{salesperson.last_name}</td>
                  <td className='text-center'>{salesperson.employee_id}</td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}
