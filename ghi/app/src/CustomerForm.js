import React, { useState } from "react";

function CustomerForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };
  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };
  const handleAddressChange = (event) => {
    const value = event.target.value;
    setAddress(value);
  };
  const handlePhoneNumberChange = (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const customer = {
      first_name: firstName,
      last_name: lastName,
      address: address,
      phone_number: phoneNumber,
    };

    const customersURL =
      "https://mar-4-et-sales.mod3projects.com/api/customers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(customer),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(customersURL, fetchConfig);
    if (response.ok) {
      setFirstName("");
      setLastName("");
      setAddress("");
      setPhoneNumber("");
    }
  };

  return (
    <div className='form-container'>
      <div className='form-card'>
        <form onSubmit={handleSubmit} id='create-customer-form'>
          <h3 className='form-title'>New Customer</h3>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleFirstNameChange}
                required
                placeholder='First Name'
                type='text'
                id='first_name'
                name='first_name'
                className='form-input'
                value={firstName}
              />
              <label htmlFor='first_name'>First Name</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleLastNameChange}
                required
                placeholder='Last Name'
                type='text'
                id='last_name'
                name='last_name'
                className='form-input'
                value={lastName}
              />
              <label htmlFor='last_name'>Last Name</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleAddressChange}
                required
                placeholder='Address'
                type='text'
                id='address'
                name='address'
                className='form-input'
                value={address}
              />
              <label htmlFor='address'>Address</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handlePhoneNumberChange}
                required
                placeholder='Phone Number'
                type='text'
                id='phone_number'
                name='phone_number'
                className='form-input'
                value={phoneNumber}
              />
              <label htmlFor='phone_number'>Phone Number</label>
            </div>
          </div>

          <div className='submit-container'>
            <button className='submit-button'>Create</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default CustomerForm;
