import { NavLink } from "react-router-dom";

function Nav() {
  // const location = useLocation();

  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-primary fixed-top'>
      <div className='container-fluid'>
        <NavLink className='navbar-brand' to='/'>
          EVOLUTION
        </NavLink>
        <button
          className='navbar-toggler'
          type='button'
          data-bs-toggle='collapse'
          data-bs-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent'
          aria-expanded='false'
          aria-label='Toggle navigation'
        >
          <span className='navbar-toggler-icon'></span>
        </button>
        <div className='collapse navbar-collapse' id='navbarSupportedContent'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item dropdown'>
              <NavLink
                className='nav-link dropdown-toggle'
                to='#'
                id='manufacturersDropdown'
                role='button'
                data-bs-toggle='dropdown'
                aria-expanded='false'
              >
                Manufacturers
              </NavLink>
              <ul
                className='dropdown-menu'
                aria-labelledby='manufacturersDropdown'
              >
                <li>
                  <NavLink className='dropdown-item' end to='/manufacturers'>
                    View Manufacturers
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    className='dropdown-item'
                    end
                    to='/manufacturers/new'
                  >
                    Add a Manufacturer
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <NavLink
                className='nav-link dropdown-toggle'
                to='#'
                id='modelsDropdown'
                role='button'
                data-bs-toggle='dropdown'
                aria-expanded='false'
              >
                Models
              </NavLink>
              <ul className='dropdown-menu' aria-labelledby='modelsDropdown'>
                <li>
                  <NavLink className='dropdown-item' end to='/models'>
                    View Models
                  </NavLink>
                </li>
                <li>
                  <NavLink className='dropdown-item' end to='/models/new'>
                    Add a Model
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <NavLink
                className='nav-link dropdown-toggle'
                to='#'
                id='automobilesDropdown'
                role='button'
                data-bs-toggle='dropdown'
                aria-expanded='false'
              >
                Automobiles
              </NavLink>
              <ul
                className='dropdown-menu'
                aria-labelledby='automobilesDropdown'
              >
                <li>
                  <NavLink className='dropdown-item' end to='/automobiles'>
                    View Automobiles
                  </NavLink>
                </li>
                <li>
                  <NavLink className='dropdown-item' end to='/automobiles/new'>
                    Add an Automobile
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <NavLink
                className='nav-link dropdown-toggle'
                to='#'
                id='techniciansDropdown'
                role='button'
                data-bs-toggle='dropdown'
                aria-expanded='false'
              >
                Technicians
              </NavLink>
              <ul
                className='dropdown-menu'
                aria-labelledby='techniciansDropdown'
              >
                <li>
                  <NavLink className='dropdown-item' end to='/technicians'>
                    View Technicians
                  </NavLink>
                </li>
                <li>
                  <NavLink className='dropdown-item' end to='/technicians/new'>
                    Add a Technician
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <NavLink
                className='nav-link dropdown-toggle'
                to='#'
                id='appointmentsDropdown'
                role='button'
                data-bs-toggle='dropdown'
                aria-expanded='false'
              >
                Appointments
              </NavLink>
              <ul
                className='dropdown-menu'
                aria-labelledby='appointmentsDropdown'
              >
                <li>
                  <NavLink className='dropdown-item' end to='/appointments'>
                    View Appointments
                  </NavLink>
                </li>
                <li>
                  <NavLink className='dropdown-item' end to='/appointments/new'>
                    Create Appointment
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    className='dropdown-item'
                    end
                    to='/appointments/history'
                  >
                    Service History
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <NavLink
                className='nav-link dropdown-toggle'
                to='#'
                id='salespeopleDropdown'
                role='button'
                data-bs-toggle='dropdown'
                aria-expanded='false'
              >
                Salespeople
              </NavLink>
              <ul
                className='dropdown-menu'
                aria-labelledby='salespeopleDropdown'
              >
                <li>
                  <NavLink className='dropdown-item' end to='/salespeople'>
                    View Salespeople
                  </NavLink>
                </li>
                <li>
                  <NavLink className='dropdown-item' end to='/salespeople/new'>
                    Add a Salesperson
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <NavLink
                className='nav-link dropdown-toggle'
                to='#'
                id='customersDropdown'
                role='button'
                data-bs-toggle='dropdown'
                aria-expanded='false'
              >
                Customers
              </NavLink>
              <ul className='dropdown-menu' aria-labelledby='customersDropdown'>
                <li>
                  <NavLink className='dropdown-item' end to='/customers'>
                    View Customers
                  </NavLink>
                </li>
                <li>
                  <NavLink className='dropdown-item' end to='/customers/new'>
                    Add a Customer
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <NavLink
                className='nav-link dropdown-toggle'
                to='#'
                id='salesDropdown'
                role='button'
                data-bs-toggle='dropdown'
                aria-expanded='false'
              >
                Sales
              </NavLink>
              <ul className='dropdown-menu' aria-labelledby='salesDropdown'>
                <li>
                  <NavLink className='dropdown-item' end to='/sales'>
                    View Sales
                  </NavLink>
                </li>
                <li>
                  <NavLink className='dropdown-item' end to='/sales/new'>
                    Create a Sale
                  </NavLink>
                </li>
                <li>
                  <NavLink className='dropdown-item' end to='/sales/history'>
                    Sales History
                  </NavLink>
                </li>
              </ul>
            </li>
            <li>
              <NavLink
                className='nav-link'
                to='/'
                id='contactDropdown'
                role='button'
                onClick={() => {
                  window.scrollTo(0, document.body.scrollHeight);
                }}
              >
                Contact Us
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
