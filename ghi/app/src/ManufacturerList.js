import React, { useState, useEffect } from "react";

async function loadData() {
  const response = await fetch(
    "https://mar-4-et-inventory.mod3projects.com/api/manufacturers/"
  );
  if (response.ok) {
    const data = await response.json();
    return data.manufacturers;
  } else {
    console.error("Failed to fetch manufacturer data");
    return [];
  }
}

function ManufacturerList() {
  const [manufacturerData, setManufacturerData] = useState([]);

  useEffect(() => {
    loadData().then((data) => {
      setManufacturerData(data);
    });
  }, []);

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Manufacturers</h3>
        <table className='table'>
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {manufacturerData.length === 0 ? (
              <tr>
                <td colSpan='4' className='text-center'>
                  No manufacturer data available
                </td>
              </tr>
            ) : (
              manufacturerData.map((manufacturer) => (
                <tr key={manufacturer.id}>
                  <td>{manufacturer.name}</td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default ManufacturerList;
