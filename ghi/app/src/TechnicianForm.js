import React, { useState } from "react";

function TechnicianForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeID, setEmployeeID] = useState("");

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleEmployeeIDChange = (event) => {
    const value = event.target.value;
    setEmployeeID(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeID;
    const techUrl =
      "https://mar-4-et-service.mod3projects.com/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(techUrl, fetchConfig);

    if (response.ok) {
      setFirstName("");
      setLastName("");
      setEmployeeID("");
    }
  };
  return (
    <div className='form-container'>
      <div className='form-card'>
        <form onSubmit={handleSubmit} id='create-technician-form'>
          <h3 className='form-title'>Add a Technician</h3>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleFirstNameChange}
                required
                placeholder='First name'
                type='text'
                id='firstName'
                name='firstName'
                className='form-input'
                value={firstName}
              />
              <label htmlFor='firstName'>First Name</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleLastNameChange}
                required
                placeholder='Last name'
                type='text'
                id='lastName'
                name='lastName'
                className='form-input'
                value={lastName}
              />
              <label htmlFor='lastName'>Last Name</label>
            </div>
          </div>

          <div className='input-container'>
            <div className='input-floating'>
              <input
                onChange={handleEmployeeIDChange}
                required
                placeholder='Employee ID'
                type='text'
                id='employeeID'
                name='employeeID'
                className='form-input'
                value={employeeID}
              />
              <label htmlFor='employeeID'>Employee ID</label>
            </div>
          </div>

          <div className='submit-container'>
            <button className='submit-button'>Create</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default TechnicianForm;
