import React, { useState, useEffect } from "react";

async function loadData() {
  const response = await fetch(
    "https://mar-4-et-service.mod3projects.com/api/technicians/"
  );
  if (response.ok) {
    const techData = await response.json();
    return techData;
  } else {
    console.error("Failed to fetch technician data");
    return null;
  }
}

function TechnicianList() {
  const [techData, setTechData] = useState([]);

  useEffect(() => {
    loadData().then((data) => {
      setTechData(data.technicians);
    });
  }, []);

  return (
    <div className='table-container'>
      <div className='table-card'>
        <h3>Technicians</h3>
        <table className='table'>
          <thead>
            <tr>
              <th className='text-center'></th>
              <th className='text-center'>Employee ID</th>
              <th className='text-center'>First Name</th>
              <th className='text-center'>Last Name</th>
            </tr>
          </thead>
          <tbody>
            {techData.length === 0 ? (
              <tr>
                <td colSpan='4' className='text-center'>
                  No technician data available
                </td>
              </tr>
            ) : (
              techData.map((tech, index) => (
                <tr key={tech.employee_id}>
                  <th className='text-center' scope='row'>
                    {index + 1}
                  </th>
                  <td className='text-center'>{tech.employee_id}</td>
                  <td className='text-center'>{tech.first_name}</td>
                  <td className='text-center'>{tech.last_name}</td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TechnicianList;
